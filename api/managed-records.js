import fetch from "../util/fetch-fill";
import URI from "urijs";

// /records endpoint
window.path = "http://localhost:3000/records";

// For the purposes of this exercise,
// the size of a given page is specified
// as 10 and is not configurable
const DEFAULT_PAGE_SIZE = 10;
const PRIMARY_COLORS = ["red", "blue", "yellow"];


/**
 * Retrieves paginated records from the /records API
 * based on page number and optional color filtration.
 * @param {Object} options - An object containing valid
 * page, color, and pageSize options, all of which are
 * optional.
 * The options object can have the following structure:
 * - page: An integer specifying which page (of size pageSize)
 *         to request. Defaults to 1.
 * - colors: An array of strings of color names that will
 *           be used to filter the records. If left empty,
 *           this function will retrieve all records regardless
 *           of color.
 * - pageSize: An integer representing the size of each page
 *             of record data. Defaults to 10.
 * @returns {Promise} - An Promise resolving to an object 
 * that contains various
 * transformations of the received records. See the function
 * comment for transformPayload() for more information about
 * its structure.
*/
const retrieve = (options) => {
    let {page, colors, pageSize} = validateOptions(options);
    let offset = (page - 1) * pageSize; // the page number is 1-indexed
    let uri = composeURI(colors, offset, pageSize);
    return fetch(uri, {
        headers: {'Content-Type': 'application/json'}})
        .then(response => {
            if(response.ok){
                return response.json();
            } else {
                throw new Error(`Invalid API response: ${response.status}: ${response.statusText} for url (${uri})`);
            }
        })
        .then(recordArray => {
            // Even though we try to retreive pageSize + 1
            // records to determine pagination, we only
            // will transform at most pageSize number of records
            let pageSlice = recordArray.slice(0, pageSize);
            let result = transformPayload(pageSlice);
            updatePagination(result, recordArray.length, page, pageSize);
            return result;
        })
        .catch(err => {
            // Per the assignment specification,
            // simply log any errors to the console
            console.log(err);
        });
};


/**
 * Composes a valid URL with formatted querystring
 * that can be used for making requests to the
 * /records API.
 * NOTE: We always ask for 1 more record than the given
 * `pageSize`. This is so we know if there is possibly
 * more pages of data to retrieve. 
 * Makes use of the URI module.
 * @param {String[]|null} colors - An array of strings
 * of color names (or null) that the API will use to
 * filter records.
 * @param {Number} offset - The index of where to start
 * querying for `pageSize` number of records in the API.
 * Defaults to 0.
 * @param {Number} pageSize - The size of the amount of
 * records to retrieve for this query. Defaults to 10.
 * @returns {URI} - A URI instance whose string representation
 * is a valid /records API request URL.
 */
const composeURI = (colors=null, offset=0, pageSize=DEFAULT_PAGE_SIZE) => {
    let uri = URI(window.path)
        .addSearch("offset", offset)
        .addSearch("limit", pageSize + 1);
    if(colors && colors.length > 0){
        uri.addSearch("color[]", colors);
    }
    return uri;
};


/**
 * Given an options object, this function
 * responds with an object that has the minimum
 * needs key/value pairs, with defaults if
 * necessary.
 * @returns {Object} - A valid options object
 * whose key/value pairs have at minimum the
 * needed defaults for a call to `receive()`
 */
const validateOptions = (options) => {
    let result = Object.assign({}, {
        page: 1,
        colors: null,
        pageSize: DEFAULT_PAGE_SIZE
    }, options);
    
    return result;
};

/**
 * Given an array of records from the /records API,
 * this function responds with an Object whose values
 * represent various transformations of the data.
 * The keys and their transformations are:
 * - ids: An array of the ids of all matched records in
 *        the payload
 * - open: All records whose disposition is 'open'
 *         NOTE: We also add a `isPrimary` property
 *         to these records
 * - closedPrimaryCount: The number of records whose
 *                       disposition is 'closed' and
 *                       whose color is a primary
 * @returns {Object}
 */
const transformPayload = (payload) => {
    let result = {
        ids: payload.map(record => { return record.id; })
    };

    // Add all records whose disposition is 'open',
    // and also add a key indicating whether or not
    // a record's color is a primary
    result.open = payload.filter(record => {
        return record.disposition == 'open';
    }).map(openRecord => {
        openRecord.isPrimary = isPrimary(openRecord.color);
        return openRecord;
    });

    // Add a count of all records whose disposition
    // is 'closed' and that also were primary colors
    result.closedPrimaryCount = payload.filter(record => {
        return (record.disposition == 'closed' && isPrimary(record.color));
    }).length;
    
    return result;
};


/**
 * Given an object that is the result of a
 * call to `transformPayload()` and payload
 * size, page number, and page size information,
 * modify the incoming `resultObject` with keys
 * indicating the `nextPage` and `previousPage`,
 * if relevant.
 * NOTE: Because all internal `fetch` requests ask for
 * `pageSize` + 1 records, knowledge of the retrieved
 * payload size relative to the page size is enough
 * information to determine if there is another page
 * to retrieve.
 * @returns {Object} resultObject - The result object, which
 * will have been modified in place.
 */
const updatePagination = (resultObject, payloadSize, pageNum, pageSize) => {
    if(payloadSize > pageSize){
        resultObject.nextPage = pageNum + 1;
    } else {
        resultObject.nextPage = null;
    }

    if(pageNum == 1){
        resultObject.previousPage = null;
    } else {
        resultObject.previousPage = pageNum - 1;
    }

    return resultObject;
};

/**
 * Returns true if the incoming color name
 * is the same as one of the primary colors.
 * @param {String} color - The name if the
 * color to compare
 * @return {Boolean} - Whether or not the
 * incoming color is a primary color.
 */
const isPrimary = (color) => {
    return PRIMARY_COLORS.includes(color);
};

export default retrieve;
